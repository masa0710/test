<!DOCTYPE html>
<html lang="jp">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- TemplateBeginEditable name="doctitle" -->
	<title>Untitled Document</title>
	<!-- TemplateEndEditable -->
	<!-- Bootstrap -->
	<link href="../bootstrap-4.3.1/css/bootstrap.css" rel="stylesheet">
	<!-- TemplateBeginEditable name="head" -->
	<!-- TemplateEndEditable -->
  </head>
  <body>
	<!--php部分--> 
    <?php
    	session_start(); 
		header('Expires:-1');
		header('Cache-Control:');
		header('Pragma:');
    ?>
    <?php include 'php/link.php';?>  
  	<!-- body code goes here -->
	<!--ヘッダー部分-->
	<?php?>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<!---->
		<!--企業ロゴ-->
       <a class="navbar-brand" href="#">企業ロゴ</a>
		<!--小さいときの右上メニューボタン-->
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
		<!--メニューの中身-->
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
			  <!--メニュー後から簡単に追加できるように-->
             <!--
             <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
             </li>
             -->
             <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
             </li>
			 <!-- 
             <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                   <a class="dropdown-item" href="#">Action</a>
                   <a class="dropdown-item" href="#">Another action</a>
                   <div class="dropdown-divider"></div>
                   <a class="dropdown-item" href="#">Something else here</a>
                </div>
             </li>
             <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
             </li>
			 --> 
          </ul>
		  <!-- 検索バーサンプル
          <form class="form-inline my-2 my-lg-0">
             <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
             <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
		  -->
       </div>
    </nav>
	<!--中身-->
	<!-- TemplateBeginEditable name="EditRegion3" -->
    
    <div class="container">
		<div class="row">
		</div>
    </div>
    <!-- TemplateEndEditable --><!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	 
	<script src="../bootstrap-4.3.1/js/jquery-3.3.1.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../bootstrap-4.3.1/js/popper.min.js"></script> 
	<script src="../bootstrap-4.3.1/js/bootstrap.js"></script>
  </body>
</html>