<!DOCTYPE html>
<html lang="jp"><!-- InstanceBegin template="/Templates/temp_login.php.dwt.php" codeOutsideHTMLIsLocked="false" -->
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>ログイン</title>
	<!-- InstanceEndEditable -->
	<!-- Bootstrap -->
	<link href="bootstrap-4.3.1/css/bootstrap.css" rel="stylesheet">
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
  </head>
  <body>
	<!--php部分--> 
    <?php
    	session_start(); 
		header('Expires:-1');
		header('Cache-Control:');
		header('Pragma:');
    ?>
    <?php include_once 'php/link.php';?>   
  	<!-- body code goes here -->
	<!--ヘッダー部分--> 
	<?php
	  
	  #ログアウトで遷移してきた場合のセッション削除
	  
	 if (isset($_POST["logout"])){
		 $_SESSION['NameId']=NULL;
		 $_SESSION['Name']="";
		 echo("logout");
	 };
	  
	  echo($_SESSION['NameId']."-I");
	  echo($_SESSION['Name']."-UN");
	  #ログイン状態確認と管理権限の再取得
	  
	  #管理者権限の初期化
	  $ManagementKey = 0 ;
	  
	  #データベース作成
	  $DatabaseConect = new DatabaseConect();
	  
	  #ログアウトで遷移された
	  if(isset($_POST["LogoutButton"])){
		  $_SESSION["NameId"]="";
		  $_SESSION['Name']="";
	  }
	   
	  #SESSIOM入っているか確認
	   if (isset($_SESSION["NameId"])){ 	
		  	#SQLによりログインする
			#SQL文
			$SQLSentence="Select * from account where NameId = " . $_SESSION["NameId"]  ;
			#SQL文セット
			$DatabaseConect->setsql($SQLSentence);
			#SQL実行
			$DatabaseConect->sqlconect();	
			#結果の取得
			$Result = $DatabaseConect->getResult();
			#G_IDの取得
			foreach ($Result as $row) {
				$ManagementKey = (int)$row["ManagementKey"];
				$NameId =$row["NameId"];
			}	
	   }
	  echo("tuka");
	  ?>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<!---->
		<!--企業ロゴ-->
       <a class="navbar-brand" href="main.php">企業ロゴ</a>
		<!--小さいときの右上メニューボタン-->
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
       </button>
		<!--メニューの中身-->
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
			  <!--メニュー後から簡単に追加できるように-->
             <!--
             <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
             </li>
             -->
			  <!--
             <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
             </li>
			 --> 
             <li class="nav-item dropdown">
				
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                管理
                </a>
				
               <div class="dropdown-menu" aria-labelledby="ManagementLabel">
				   <?php
				   switch($ManagementKey){
					   case 0:
				   ?>
				   <a class="dropdown-item" href="login.php">ログイン</a>
				   <?php
						   break;
					   case 1:
				   ?>
				   <form id = "Logoutform" name="LogoutForm" method="post" action="login.php" onSubmit="return logoutcheck()" >
					   <input class="dropdown-item" type="submit" name="LogoutButton" value="ログアウト">
					   <input type ="hidden" name = "logout" value ="true">
				   </form>							
				   <div class="dropdown-divider"></div>
				   <form id = "usereditform" name="UserEditForm" method="post" action="useredit.php">
					   <input type="hidden" name="UserID" value="<?php echo($NameId) ?>">
					   <input class="dropdown-item" type="submit" name="UserEditButton" value="アカウント編集">
				   </form>	
				   <?php
						   break;
					   case 2:
				   ?>
				   <form id = "Logoutform2" name="LogoutForm" method="post" action="login.php" onSubmit="return logoutcheck()" > 
					   <input class="dropdown-item" type="submit" name="LogoutButton2" value="ログアウト">
					   <input type ="hidden" name = "logout" value ="true">
				   </form>
				   <div class="dropdown-divider"></div>
				   <a class="dropdown-item" href="userlist.php">ユーザーリスト</a> 
				   <?php
						   break;
				   }
				   ?>
				 </div>
			  </li>
			  <!--
             <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
             </li>
			 --> 
          </ul>
		  <!-- 検索バーサンプル
          <form class="form-inline my-2 my-lg-0">
             <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
             <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
		  -->
       </div>
    </nav>
	<!--中身-->
	<!-- InstanceBeginEditable name="EditRegion3" -->
	
	  
	<!--PHPlogin-->  
	<?php
	  #データベース作成
	  $DatabaseConect =new DatabaseConect();
	  #hash確認  	  
	  if (isset($_POST["hash"])){
		  #hashが自分のか確認
		  if ($DatabaseConect->validate_token($_POST["hash"])){
			  #hashOK!
			  #SQLによりログインする
			  #SQL文
			  $SQLSentence="Select * from account where Id = '" . $_POST["inputId"] . "' and Password = '". ($_POST["inputPassword"]) ."'";
			  #SQL文セット
			  $DatabaseConect->setsql($SQLSentence);
			  #SQL実行
			  $DatabaseConect->sqlconect();	
			  #結果の取得
			  $Result = $DatabaseConect->getResult();
			  #G_IDの取得
			  foreach ($Result as $row) {
				  $NameId = (int)$row["NameId"];
				  #echo("set NameId = " . $NameId);
				  $Name = $row["Name"];						  	
			  }	
			  if(isset($NameId)){			  
				  $_SESSION['NameId']=$NameId;
				  $_SESSION['Name']=$Name;
				  $LoginErrorMessage="";
				  header('location:/main.php');
				  echo($NameId."-N" );
				  echo($_SESSION['NameId']."-S");
				  echo($Name."-N" );
				  echo($_SESSION['Name']."-S");
				  
			  }else{
				  $LoginErrorMessage="IDまたはパスワードが間違っています";
			  }	
		  }else{
			  #hashOut
		  }
	  }

	?>
    <div class="container">
		<div class="row">
			<!--画面位置サイズ調整-->
			<div class="d-none d-md-block col-md-1 col-lg-2 col-xl-3"></div>
			<div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
				<form class="" id="" name ="LoginForm" method="post" action="">	
					<!--入力項目-->
					<div class="row" style="margin: 25px 0 0 ">
						<div class="col-md-3">
							<p id = 'test'>ID</p>
						</div>
						<div class="col-md-9">
							<input type="text" class="form-control" name="inputId" placeholder="ID"  required>
						</div>
						<div class="col-md-3">
							<p id = 'test'>PassWord</p>
						</div>
						<div class="col-md-9">
							<input type="text" id="inputNumber" name="inputPassword" class="form-control" placeholder="Password" required>
							<input type="hidden" name="hash" value="<?php echo($DatabaseConect->generate_token());?>">
						</div>						
					</div>
					<!--ボタン-->
					<div class="row" style="margin: 25px 0 0 ">
						<div class="col-md-6">
							<input type ="submit" name="loginbutton" value="login" >
						</div>
						<div class="col-md-6">
							<a href="#">アカウントお持ちでない方</a>
						</div>
					</div>
					<div class="row">
						<div class="col text-center">
							<p>
								<?php if(isset($LoginErrorMessage))
									{
										echo($LoginErrorMessage) ;
									}
								?>
							</p>
						</div>
					</div>
				</form>
			</div>				
			<div class="d-none col-md-1 col-lg-2 col-xl-3"></div>
		</div>	
    </div>
	
    <!-- InstanceEndEditable --><!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	 
	<script src="bootstrap-4.3.1/js/jquery-3.3.1.min.js"></script> 

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<!--<script src="../bootstrap-4.3.1/js/popper.min.js"></script> -->
	<script src="bootstrap-4.3.1/js/bootstrap.js"></script>
	  
	 <!--Jquery用-->
	  <!--ログアウト用-->
	  <script>
		  function logoutcheck(){
			  if(confirm('本当にログアウトしますか？')){
				  /* キャンセルの時の処理 */
				  return true;
			  }else{
				  return false;
			  }
		  };
	  </script>
	  
	 <!-- InstanceBeginEditable name="EditRegion4" -->
	<script type ="text/jscript">
	  	$('#test').click(function () {
    		$(this).text("IDを入力してください");
  		});
		/*高さ取得*/
		$(document).ready(function () {
			hsize = $(window).height();
			$("section").css("height", hsize + "px");
		});

	</script>
	  
	 <!-- InstanceEndEditable -->
  </body>
<!-- InstanceEnd --></html>