<?php 
##JsonFile作成用ファイル　
#概略[GET]→[PHP]→[SQL]→[JSON]
#取扱い
#[GET]においての必要項目
#-SQL実行のため [SQLSentence]が必要
#-結果を受けるために[result]が必要
##[SQLSentence]の定義
##通用するものを用意
##[result]の定義
##配列で格納する
#（例）と注意
##-- http://localhost/jsonLinkSQLphp?SQLsentence=select+*+from+account&&result[]=name&&result[]=nameId”
##(空白)→（+）、（日本語）→（％＊＊）となる
##配列の格納にはしっかり[]をつける

#結果戻り値の変数
$ResultSentence= "";
#GET取得
$SQLSentence = $_GET["SQLSentence"];
$GetResult = $_GET["Result"];
#SQL文の有無を確認
if (isset($SQLSentence)){
	#SQL文があれば
	#Class ファイル呼び出し
	include_once 'php/link.php';
	#クラス作成
	$DatabaseConect =new DatabaseConect();
	#SQL文の取得
	#クラスにセット
	$DatabaseConect->setsql($SQLSentence);
	#実行
	$DatabaseConect->sqlconect();
	#実行結果の取得
	$Result = $DatabaseConect->getResult();

	
	#テーブルの最初の囲いを用意
	$ResultSentence = $ResultSentence.'[';
	#結果の取得
	foreach($Result as $value){
		#Result(テーブルの状態)→value（一行に並んでる状態）		
		#最終結果文を用意
		if (isset($GetResult)){
			#resultがGETで送られてきている場合
			#行の最初の囲い
			$ResultSentence = $ResultSentence.'{"';
			#中身
			foreach($GetResult as $ResultValue ){
				#ほしいデータを呼び出して戻す
				$ResultSentence = $ResultSentence.$ResultValue.'":"'.$value[$ResultValue].'",';
			};	
			#最後にいらない,を削除する
			$ResultSentence = rtrim($ResultSentence, ',');
			#行の最後の囲い
			$ResultSentence = $ResultSentence.'},';
		}else{
			#ResultがGETで送られてきていない場合
			$ResultSentence = "{Result:EmptyResult},";
		};		
	};	
	#最後にいらない,を削除する
	$ResultSentence = rtrim($ResultSentence, ',');
	#テーブルの最後の囲いを用意
	$ResultSentence = $ResultSentence."]";
}else{
	#SQL文がGETで送られてきてなかった場合
	$ResultSentence = "{Result:EmptySQL}";
};
#結果の表示
echo($ResultSentence);
?>